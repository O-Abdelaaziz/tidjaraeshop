export class ProductDetail {
  id: number;
  productId: number;
  metaKey: string;
  metaValue: string;
}
