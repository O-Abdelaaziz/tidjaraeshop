import { ProductDetail } from './../shchema/product-detail';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

interface GetEmbeddedProductDetails{
  _embedded: {
    productDetails: ProductDetail[];
  }
}

@Injectable({
  providedIn: 'root'
})
export class ProductDetailService {

  baseUrl:string="http://192.168.40.98:8081/productDetails";

  constructor(private http: HttpClient) { }

  getAllProductByProductId(productId:number): Observable<GetEmbeddedProductDetails> {
   const searchUrl=`${this.baseUrl}/search/findByProductId?productId=${productId}`;
   return this.http.get<GetEmbeddedProductDetails>(searchUrl);
 }
}
