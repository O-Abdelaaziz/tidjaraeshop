import { ProductCategory } from './../shchema/product-category';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface GetEmbeddedCategories{
  _embedded: {
    categoryLists: ProductCategory[];
  },
  page:{
    size:number,
    totalElements:number,
    totalPages:number,
    number:number
  }
}

@Injectable({
  providedIn: 'root',
})
export class ProductCategoryService {

  parentsBaseUrl:string="http://192.168.40.98:8081/categoryLists";
  baseUrl: string ='http://192.168.40.98:8081/categoryLists/search/findByIdCategorieParant?parenid=0';

  constructor(private http: HttpClient) {}

  getAllCategories(): Observable<GetEmbeddedCategories> {
    const searchUrl = `${this.baseUrl}`;
    return this.http.get<GetEmbeddedCategories>(searchUrl);
  }

  getAllParentsCategories(categoryParentId: number): Observable<ProductCategory[]> {
    const searchUrl = `${this.parentsBaseUrl}/search/findByIdCategorieParant?parenid=${categoryParentId}`;
    return this.http
      .get<GetEmbeddedCategories>(searchUrl)
      .pipe(map((response) => response._embedded.categoryLists));
  }
}
