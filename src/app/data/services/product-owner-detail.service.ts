import { ProductOwnerDetail } from './../shchema/product-owner-detail';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

interface GetEmbeddedOwnerDetails{
  _embedded: {
    productOwnerDetails: ProductOwnerDetail[];
  }
}

@Injectable({
  providedIn: 'root'
})
export class ProductOwnerDetailService {

  baseUrl:string="http://192.168.40.98:8081/productOwnerDetails";

   constructor(private http: HttpClient) { }

   getOwnerInfoByProductId(productId:number): Observable<GetEmbeddedOwnerDetails> {
    const searchUrl=`${this.baseUrl}/search/findByProductId?productId=${productId}`;
    return this.http.get<GetEmbeddedOwnerDetails>(searchUrl);
  }

  getOwnerInfoByOwnerId(ownerId:number): Observable<GetEmbeddedOwnerDetails> {
    const searchUrl=`${this.baseUrl}/search/findByOnwerId?ownerId=${ownerId}`;
    return this.http.get<GetEmbeddedOwnerDetails>(searchUrl);
  }
}
