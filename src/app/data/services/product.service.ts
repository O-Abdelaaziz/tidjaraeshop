import { Product } from './../shchema/product';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

interface GetEmbeddedBooks {
  _embedded: {
    productLists: Product[];
  };
  page: {
    size: number;
    totalElements: number;
    totalPages: number;
    number: number;
  };
}

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  baseUrl: string = 'http://192.168.40.98:8081/productLists';

  constructor(private http: HttpClient) {}

  getAllProducts(
    categoryId: number,
    currentPage: number,
    pageSize: number
  ): Observable<GetEmbeddedBooks> {
    const searchUrl = `${this.baseUrl}/search/findByIdcategorie?idcategorie=${categoryId}&page=${currentPage}&size=${pageSize}`;
    return this.http.get<GetEmbeddedBooks>(searchUrl);
  }

  getAllProductsByName(
    name: string,
    currentPage: number,
    pageSize: number
  ): Observable<GetEmbeddedBooks> {
    const searchUrl = `${this.baseUrl}/search/findByPostTitleContainingIgnoreCase?name=${name}&page=${currentPage}&size=${pageSize}`;
    return this.http.get<GetEmbeddedBooks>(searchUrl);
  }

  getProductById(id: number): Observable<Product> {
    const searchUrl = `${this.baseUrl}/${id}`;
    return this.http.get<Product>(searchUrl);
  }
}
