import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeatureItemsComponent } from './components/feature-items/feature-items.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductsComponent } from './components/products/products.component';
import { LandingComponent } from './landing.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    children: [
      { path: 'products', component: ProductsComponent },
      {path: 'products', component: ProductsComponent},
      {path: 'search/:name', component: ProductsComponent},
      {path: 'category/:id', component: ProductsComponent},
      { path: 'feature-items', component: FeatureItemsComponent },
      { path: 'product-details/:id', component: ProductDetailsComponent },
      { path: '', redirectTo: 'feature-items', pathMatch: 'full'}
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LandingRoutingModule {}
