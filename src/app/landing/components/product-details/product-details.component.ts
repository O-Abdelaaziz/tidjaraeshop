import { ProductOwnerDetailService } from './../../../data/services/product-owner-detail.service';
import { ProductOwnerDetail } from './../../../data/shchema/product-owner-detail';
import { ProductDetailService } from './../../../data/services/product-detail.service';
import { ProductDetail } from './../../../data/shchema/product-detail';
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/data/shchema/product';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/data/services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  products: Product = new Product();
  productDetails: ProductDetail[];
  productOwnerDetail: ProductOwnerDetail[];
  productId: number;
  mapProduct = new Map<string, string>();
  mapOwner = new Map<string, string>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private productDetailsService: ProductDetailService,
    private productOwnerDetailsService: ProductOwnerDetailService,
    private productService: ProductService
  ) {}


  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(() => {
      this.selectProductById();
      this.selectProductDetailById();
      //this.selectOwnerDetailById();
    });
  }

  selectProductById() {

    this.productId = +this.activatedRoute.snapshot.paramMap.get('id');
 
    console.log('id ' + this.productId);

    this.productService
      .getProductById(this.productId)
      .subscribe((product) => {
        this.products = product;
        console.log('book ' + this.products);
      });
  }

  selectProductDetailById() {
    this.productDetailsService
      .getAllProductByProductId(this.productId)
      .subscribe((product) => {
        this.productDetails = product._embedded.productDetails;
        for (let p of this.productDetails) {
          this.mapProduct.set(p.metaKey, p.metaValue);
        }
      });
  }

  selectOwnerDetailById(id: number) {
    this.productOwnerDetailsService
      .getOwnerInfoByOwnerId(id)
      .subscribe((ownerDetails) => {
        this.productOwnerDetail = ownerDetails._embedded.productOwnerDetails;
          console.log('productOwnerDetail' + this.productOwnerDetail);
        for (let p of this.productOwnerDetail) {
          this.mapOwner.set(p.metaKey, p.metaValue);
        }
      });
  }

}
