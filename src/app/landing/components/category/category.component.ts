import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ProductCategoryService } from 'src/app/data/services/product-category.service';
import { ProductCategory } from 'src/app/data/shchema/product-category';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
})
export class CategoryComponent implements OnInit {
  categories: ProductCategory[];
  categoriesParent: ProductCategory[] = [];
  constructor(
    private productCategoryService: ProductCategoryService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.selectAllCategories();
  }

  selectAllCategories() {
    this.productCategoryService.getAllCategories().subscribe((categories) => {
      this.categories = categories._embedded.categoryLists;
    });
  }

  selectAllCategoriesParentById(cateParentId: number) {
    this.productCategoryService
      .getAllParentsCategories(cateParentId)
      .subscribe((categoriesparent) => {
        if(categoriesparent.length > 0){
          this.categoriesParent = categoriesparent;
          console.log(' this.listCategoriesParent ' + categoriesparent.length);
        }else{
          this.showWarning("There is no sub category for category selected","Warning")
        }

      });
  }

  showWarning(message, title){
    this.toastr.warning(message, title)
}
}
