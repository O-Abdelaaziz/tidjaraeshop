import { ProductService } from './../../../data/services/product.service';
import { Product } from './../../../data/shchema/product';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  products: Product[];
  categoryId: number;
  postTitle: string;
  searchMode: boolean;

  currentPage: number = 1;
  pageSize: number = 5;
  totalItems: number = 0;
  previousCategory: number = 2021;

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private spinnerService: NgxSpinnerService,
    private router: Router,
    private ngbPaginationConfig: NgbPaginationConfig
  ) {
    ngbPaginationConfig.maxSize = 3;
    ngbPaginationConfig.boundaryLinks = true;
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(
      () => {
        this.selectAllProducts();
      }
    );
  }

  selectAllProducts() {
    this.spinnerService.show();
    this.searchMode = this.activatedRoute.snapshot.paramMap.has('name');
    console.log("this.searchMode " +this.searchMode);
    
    if (this.searchMode) {
      this.selectAllProductsByName();
    }else{
      this.selectAllProductsById();
    }
  }

  selectAllProductsById(){

    const hasCategoryId: boolean = this.activatedRoute.snapshot.paramMap.has('id');
    console.log("has "+hasCategoryId);

    if (hasCategoryId) {
      this.categoryId = +this.activatedRoute.snapshot.paramMap.get('id');
    } else {
      this.categoryId = 2021;
    }
    
    if (this.previousCategory != this.categoryId) {
      this.currentPage = 1;
    }

    this.previousCategory = this.categoryId;

    this.productService.getAllProducts(this.categoryId,this.currentPage - 1, this.pageSize).subscribe(
      this.loadBookWithSppiner()
    );
  }


  selectAllProductsByName() {
    this.postTitle = this.activatedRoute.snapshot.paramMap.get('name');
    // console.log("book name "+this.postTitle);

    this.productService.getAllProductsByName(this.postTitle,this.currentPage - 1, this.pageSize).subscribe(
      this.loadBookWithSppiner()
    );
  }



  loadBookWithSppiner() {
    return (productsList) => {
      // console.log("allproducts" + productsList);
      
      this.spinnerService.hide();
      this.products = productsList._embedded.productLists;
      this.currentPage = productsList.page.number + 1;
      this.totalItems = productsList.page.totalElements;
      this.pageSize = productsList.page.size;

    }
  }


  updatePageSize(pageSize: number) {
    this.pageSize = pageSize;
    this.currentPage = 1;
    this.selectAllProducts();
  }
}
