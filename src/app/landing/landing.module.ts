import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';
import { HeaderComponent } from './partial/header/header.component';
import { SliderComponent } from './partial/slider/slider.component';
import { FooterComponent } from './partial/footer/footer.component';
import { CategoryComponent } from './components/category/category.component';
import { CategoryTabComponent } from './components/category-tab/category-tab.component';
import { ProductsComponent } from './components/products/products.component';
import { FeatureItemsComponent } from './components/feature-items/feature-items.component';
import { RecommendedItemsComponent } from './components/recommended-items/recommended-items.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [LandingComponent, HeaderComponent, SliderComponent, FooterComponent, CategoryComponent, FeatureItemsComponent, CategoryTabComponent, RecommendedItemsComponent, ProductDetailsComponent, ProductsComponent],
  imports: [
    CommonModule,
    LandingRoutingModule,
    NgbModule,
    NgxSpinnerModule,
  ]
})
export class LandingModule { }
